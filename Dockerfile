FROM ruby:alpine
RUN apk add --update build-base libffi-dev ruby-dev
RUN gem install ffi
RUN gem install fastlane -NV

# Set environment variables for fastlane
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8

ENTRYPOINT ["fastlane"]
